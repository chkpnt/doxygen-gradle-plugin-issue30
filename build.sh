#!/usr/bin/env bash
set -x

export

iptables -A OUTPUT -p tcp --dport 443 -j DROP
iptables -A OUTPUT -p tcp --dport 80 -j DROP

# Uncomment, if an interactive session into the docker container is needed:
#tail -f /dev/null

./gradlew doxygen
